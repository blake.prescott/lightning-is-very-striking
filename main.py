#!/bin/python3

# Dependancies
import json
import requests
import datetime
import psycopg2
from time import time

# Return readable time or seconds since epoch 
def get_timestamp(time_type=None):
    if time_type == 'now':
        return datetime.datetime.now()
    else:
        return time()

# Get weather info from open weather map. Grab relevant information
def fetch_weather():
    weather_status = requests.get('https://api.openweathermap.org/data/2.5/weather?q=Portland&appid=ce4a17609dc058a2a531ccfdd324b272', stream = True)
    full_weather = json.loads(weather_status.content)
    current_city = full_weather['name']
    # Convert from nerd units to freedom units
    in_fahrenheit = round(9/5 * full_weather['main']['temp'] - 459.67, 2)

    return {'City': current_city, 'Temperature': in_fahrenheit}

def insert_into_table(ts, city, temp):
    dbconn = psycopg2.connect(database = 'weather_cache', user = 'weather')
    dbcur = dbconn.cursor()
    
    dbcur.execute("INSERT INTO history (timestamp, city, temp) VALUES ("+str(ts)+", '"+str(city)+"', "+str(temp)+")")
    
    dbconn.commit()
    
def check_last_pull(city='Portland'):
    dbconn = psycopg2.connect(database = 'weather_cache', user = 'weather')
    dbcur = dbconn.cursor()
    
    dbcur.execute('SELECT "timestamp" FROM history ORDER BY "timestamp" DESC LIMIT 1;')
    if dbcur.rowcount == 0:
        return 0
    else:
        # Item is a list of a single tuple, containing a single data point. 
        return dbcur.fetchall()[0][0]

def fetch_from_db():
    dbconn = psycopg2.connect(database = 'weather_cache', user = 'weather')
    dbcur = dbconn.cursor()
    
    dbcur.execute('SELECT * FROM history ORDER BY "timestamp" DESC LIMIT 1;')
    return dict(zip(('ts', 'city', 'temperature'), dbcur.fetchall()[0]))
