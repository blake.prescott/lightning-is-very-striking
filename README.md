To deploy project: 
    mkdir ~/weather_cache_app
    cd ~/weather_cache_app && git clone https://gitlab.com/blake.prescott/lightning-is-very-striking.git
    *** Edit the Vagrantfile, verify or change config.vm.provider ***
    *** change config.vm.network "public_network" to reflect your environment***
    vagrant up

To access API:
    From Host: curl http://0.0.0.0:8080/temperature

Example of output:
    {"City":"Portland","Temperature":80.01,"query_time":"Thu, 01 Jul 2021 17:11:24 GMT"}

Possible issues: 
    On 3 tests, the flask app did not run. 
        to run: vagrant ssh
                sudo -u weather python /home/vagrant/weather_cache_app/serve_api.py &
