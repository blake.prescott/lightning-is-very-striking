#!/bin/python3

# Dependancies 
import json
from flask import Flask, jsonify, Response, request
import main

app = Flask(__name__)

@app.route("/temperature", methods=['GET'])
def display_temp():
    current = main.get_timestamp()
    last_pull = main.check_last_pull(current)
    if request.method == 'GET':
        if round(current) - round(float(last_pull)) > 300:
            current_weather = {'ts': main.get_timestamp(), 'City': main.fetch_weather()['City'], 'Temperature': main.fetch_weather()['Temperature']}
            main.insert_into_table(current_weather['ts'], current_weather['City'], current_weather['Temperature'])
            del current_weather['ts']
            current_weather.update({'query_time': main.get_timestamp('now')})
            return jsonify(current_weather)
        else:
            current_weather = main.fetch_from_db()
            del current_weather['ts']
            current_weather.update({'query_time': main.get_timestamp('now')})
            return jsonify(current_weather)
    else:
        return Response("{'a':'b'}", status = 401, mimetype = 'application/json')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5050')
