exec { 'pip install flask':
    path => ['/usr/bin'],
    require => Exec['pip install --upgrade "pip < 20.0"'],
}
exec { 'pip install requests':
    path => ['/usr/bin'],
    require => Exec['pip install --upgrade "pip < 20.0"'],
}
exec { 'pip install psycopg2-binary':
    path => ['/usr/bin'],
    require => Package['postgresql11-devel'],
}
exec { 'pip install --upgrade "pip < 20.0"':
    path => ['/usr/bin'],
}

exec { 'yes | sudo yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm' :
    path => ['/bin'],
    require => Exec['pip install --upgrade "pip < 20.0"'],
}
package { 'postgresql11-server' :
    ensure => latest,
    require => Exec['yes | sudo yum install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm'],
}
package { 'postgresql11-contrib':
    ensure => latest,
    require => Package ['postgresql11-server'],
}
package { 'centos-release-scl' :
    ensure => latest,
    require => Package ['postgresql11-server'],
}
package { 'postgresql11-devel':
    ensure => latest,
    require => Package ['centos-release-scl'],
}

package { 'git' :
    ensure => latest,
}

exec { 'sudo /usr/pgsql-11/bin/postgresql-11-setup initdb' :
    path => ['/usr/bin'],
    require => Package['postgresql11-contrib', 'postgresql11-devel'],
}

exec { 'sudo systemctl start postgresql-11 && sudo systemctl enable postgresql-11' :
    path => ['/usr/bin'],
    require => Exec['sudo /usr/pgsql-11/bin/postgresql-11-setup initdb'],
}

exec { 'sudo -u postgres createdb weather_cache' :
    path => ['/usr/bin'],
    require => Exec['sudo systemctl start postgresql-11 && sudo systemctl enable postgresql-11'],
}

exec { 'sudo -u postgres psql -d weather_cache -c "create user weather with password \'W3ath3r\'"' :
    path => ['/usr/bin'],
    require => Exec['sudo -u postgres createdb weather_cache'],
}

exec { 'sudo -u postgres psql -d weather_cache -c "alter role weather with superuser;"' :
    path => ['/usr/bin'],
    require => Exec['sudo -u postgres psql -d weather_cache -c "create user weather with password \'W3ath3r\'"'],
}

exec { 'sudo -u postgres psql -d weather_cache -c "create table history(timestamp VARCHAR(255), city VARCHAR(255), temp VARCHAR(255))"' :
    path => ['/usr/bin'],
    require => Exec['sudo -u postgres psql -d weather_cache -c "create user weather with password \'W3ath3r\'"'],
}

exec { 'sudo -u postgres psql -d weather_cache -c "grant all privileges on database weather_cache to weather"' :
    path => ['/usr/bin'],
    require => Exec['sudo -u postgres psql -d weather_cache -c "create table history(timestamp VARCHAR(255), city VARCHAR(255), temp VARCHAR(255))"'],
}

file { '/home/vagrant/weather_cache_app' :
    ensure => 'directory',
    require => Exec['sudo -u postgres psql -d weather_cache -c "grant all privileges on database weather_cache to weather"'],
}

exec { 'git clone https://gitlab.com/blake.prescott/lightning-is-very-striking.git /home/vagrant/weather_cache_app' :
    path => ['/usr/bin'],
    onlyif => 'test -d /home/vagrant/weather_cache_app',
    require => File['/home/vagrant/weather_cache_app'],
}

exec { 'sudo useradd weather' :
    path => ['/usr/bin'],
    require => Exec['git clone https://gitlab.com/blake.prescott/lightning-is-very-striking.git /home/vagrant/weather_cache_app'],
}

exec { 'sudo chmod 777 /home/vagrant/weather_cache_app/*.py' :
    path => ['/usr/bin'],
    require => Exec['sudo useradd weather'],
}

exec { 'sudo -u weather python /home/vagrant/weather_cache_app/serve_api.py &' :
    path => ['/usr/bin'],
    require => Exec['sudo firewall-cmd --reload', 'sudo chmod 777 /home/vagrant/weather_cache_app/*.py'],
}

exec { 'sudo firewall-cmd --permanent --zone=public --add-port=5050/tcp' :
    path => ['/usr/bin'],
    require => Exec['git clone https://gitlab.com/blake.prescott/lightning-is-very-striking.git /home/vagrant/weather_cache_app'],
}

exec { 'sudo firewall-cmd --reload' :
    path => ['/usr/bin'],
    require => Exec['sudo firewall-cmd --permanent --zone=public --add-port=5050/tcp'],
}
